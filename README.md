<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Описание

- web - роуты, которые использует банк для отправки данных по платежам
- api - роут для создания оплаты
- app/Http/Payment - обработка запросов с банка
- app/Http/API/PaymentController - колбек для создания оплаты
- app/Http/Requests - валидация запросов
- app/Http/Middleware/Payment - логирование запросов с банка
- app/Payment - реализация логики типов оплаты
- app/Service/PaymentService.php - сервис для работы с типами оплаты

<?php

use App\Http\Controllers\Payment\LiqpayController;
use App\Http\Controllers\Payment\MonoController;
use App\Http\Controllers\Payment\PrivatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'payment', 'middleware' => ['cors', 'payment']], function () {
    Route::post('liqpay/process', [LiqpayController::class, 'processPayment'])->name('payment.liqpay.process');
    Route::post('privat/process', [PrivatController::class, 'processPayment'])->name('payment.privat.process');
    Route::post('mono/process', [MonoController::class, 'processPayment'])->name('payment.mono.process');
});

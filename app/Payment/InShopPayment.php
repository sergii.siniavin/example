<?php

namespace App\Payment;

use Illuminate\Contracts\Container\BindingResolutionException;

class InShopPayment extends BasePayment
{
    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        parent::__construct();

        $this->config = config('payment.inShop');
        $this->paymentReturnType = $this->config['payment_return_type'] ?: $this->paymentReturnType;
    }

    /**
     * @param array $data
     * @return array{success:bool, type:string, data:string|array}
     */
    public function createPayment(array $data): array
    {
        $this->createLog(['description' => "Платеж создан для типа оплаты {$this->order->paymentType?->name_ru}"]);

        return [
            'success' => true,
            'type' => $this->paymentReturnType,
            'data' => []
        ];
    }

    /**
     * @return array
     */
    public function getPaymentStatus(): array
    {
        return [
            'success' => true,
            'is_payed' => $this->order->payment->hold_completion,
            'is_hold' => false,
            'is_returned' => false,
            'data' => []
        ];
    }

    /**
     * @param array $data
     * @return void
     */
    public function processPayment(array $data): void
    {
        $this->createLog(['description' => "Не требуется обработка платежа для типа оплаты {$this->order->paymentType?->name_ru}"]);
    }

    /**
     * @param int|null $sum
     * @return array
     */
    public function holdCompletion(int $sum = null): array
    {
        $this->createLog(['description' => "Не требуется подтверждение платежа для типа оплаты {$this->order->paymentType?->name_ru}"]);
        return [];
    }

    /**
     * @return array
     */
    public function refundPayment(): array
    {
        $this->createLog(['description' => "Не требуется возврат платежа для типа оплаты {$this->order->paymentType?->name_ru}"]);

        return [
            'success' => true
        ];
    }

    /**
     * @return array
     */
    public function rejectPayment(): array
    {
        $this->createLog(['description' => "Не требуется отмена платежа для типа оплаты {$this->order->paymentType?->name_ru}"]);

        return [
            'success' => true
        ];
    }
}

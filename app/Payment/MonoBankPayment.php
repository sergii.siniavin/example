<?php

namespace App\Payment;

use App\Models\Order;
use App\Models\OrderPayment;
use App\Models\PaymentStatus;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Log;

class MonoBankPayment extends BasePayment
{
    public function __construct()
    {

    }

    /**
     * @return array
     */
    public function getPaymentStatus(): array
    {

    }

    /**
     * @param array $data
     * @return array{success:bool, type:string, data:string|array}
     * @throws Exception
     */
    public function createPayment(array $data): array
    {

    }

    /**
     * @param array $data
     * @return void
     */
    public function processPayment(array $data): void
    {

    }

    /**
     * @return array
     */
    public function holdCompletion(): array
    {

    }

    /**
     * @return array
     * @throws Exception
     */
    public function rejectPayment(): array
    {

    }

    /**
     * @param int|null $sum
     * @return array
     * @throws Exception
     */
    public function refundPayment(int $sum = null): array
    {

    }

    /**
     * @return array
     */
    private function generateProductsData(): array
    {

    }

    /**
     * @param string $url
     * @param array $data
     * @return mixed
     */
    private function request(string $url, array $data): mixed
    {

    }
}

<?php

namespace App\Payment;

use App\OneC\OneCOrder;
use App\Models\Order;
use App\Models\OrderPayment;
use App\Models\PaymentStatus;
use App\Models\PaymentType;
use App\Models\PaymentTypeLog;
use App\Repositories\OrderPaymentRepository;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Log;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

abstract class BasePayment implements PaymentStrategy
{
    /**
     * @var Order $order
     */
    protected Order $order;

    /**
     * @var string $language
     */
    protected string $language;

    /**
     * @var string $paymentReturnType
     */
    protected string $paymentReturnType = 'redirect';

    /**
     * @var string|null $paymentOrderId
     */
    protected ?string $paymentOrderId = null;

    /**
     * @var OrderPayment|null
     */
    protected ?OrderPayment $payment = null;

    /**
     * @var string $baseURL
     */
    protected string $baseURL;

    /**
     * @var array $config
     */
    protected array $config;

    /**
     * @var array $paymentOneCData
     */
    protected array $paymentOneCData = [];

    /**
     * @var array $paymentInfo
     */
    protected array $paymentInfo = [];

    /**
     * @var array $settings
     */
    protected array $settings = [];

    /**
     * @var PaymentType|null $paymentType
     */
    public ?PaymentType $paymentType = null;

    /**
     * @var OrderPaymentRepository
     */
    protected OrderPaymentRepository $orderPaymentRepository;

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->baseURL = config('app.url');
        $this->setLanguage();
        $this->orderPaymentRepository = app()->make(OrderPaymentRepository::class);
    }

    abstract public function createPayment(array $data): array;

    abstract public function getPaymentStatus(): array;

    abstract public function processPayment(array $data): void;

    abstract public function holdCompletion(): array;

    abstract public function refundPayment();

    abstract public function rejectPayment();

    /**
     * @param Order $order
     * @return void
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    /**
     * @param array $data
     * @return void
     */
    public function setPaymentSettings(array $data): void
    {
        $this->settings = $data;
    }

    /**
     * @return bool
     */
    public function needsRedirectUrl(): bool
    {
        return false;
    }

    /**
     * @param string $value
     * @return void
     */
    public function setPaymentOrderId(string $value): void
    {
        $this->paymentOrderId = $value;

        if ($this->payment && $this->payment->payment_order_id === $this->paymentOrderId) {
            return;
        }

        $this->setPayment($this->paymentOrderId);
    }

    /**
     * @param string|OrderPayment|null $payment
     * @return void
     */
    public function setPayment(string|OrderPayment $payment = null): void
    {
        if ($payment instanceof OrderPayment) {
            $this->payment = $payment;
            $this->setPaymentOrderId($this->payment->payment_order_id);
            return;
        }

        if (is_string($payment)) {
            $this->payment = $this->orderPaymentRepository->findByField('payment_order_id', $payment);

            if ($this->payment) {
                $this->setPaymentOrderId($this->payment->payment_order_id);
            }

            return;
        }

        if (!$this->getPaymentOrderId()) {
            return;
        }

        $this->payment = $this->orderPaymentRepository->findByField('payment_order_id', $this->getPaymentOrderId());

        if ($this->payment) {
            $this->setPaymentOrderId($this->payment->payment_order_id);
        }
    }

    /**
     * @return OrderPayment|null
     */
    public function getPayment(): ?OrderPayment
    {
        return $this->payment;
    }

    /**
     * @return string|null
     */
    public function getPaymentOrderId(): ?string
    {
        return $this->paymentOrderId;
    }

    /**
     * @return string
     */
    public function getOrderPrefix(): string
    {
        return '';
    }

    /**
     * @return string
     */
    protected function getWhoUpdatedText(): string
    {
        $name = $this->config['name'] ?? '';
        return "Платежная система ($name)";
    }

    /**
     * @param PaymentType $paymentType
     * @return void
     */
    public function setPaymentType(PaymentType $paymentType): void
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return PaymentType|null
     */
    public function getPaymentType(): ?PaymentType
    {
        return $this->paymentType;
    }

    /**
     * @return void
     */
    protected function setLanguage(): void
    {
        try {
            $this->language = session()->get('locale') ?? 'ua';
        } catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {
            Log::error($e->getMessage(), $e->getTrace());
            $this->language = 'ua';
        }

        if (!in_array($this->language, ['ru', 'ua', 'en'])) {
            $this->language = 'ua';
        }
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param array $data
     * @return void
     */
    protected function createLog(array $data): void
    {
        PaymentTypeLog::createLog([
            'order_id' => $this->order->id,
            'payment_type_id' => $this->getPaymentType() ? $this->getPaymentType()->id : $this->order->paymentType->id,
            'description' => $data['description'] ?? null,
            'request' => $data['request'] ?? null,
            'response' => $data['response'] ?? null
        ]);
    }

    /**
     * Send payment data to 1C
     *
     * @return void
     * @throws BindingResolutionException
     */
    protected function sendPaymentToOneC(): void
    {
        Log::info("Sending payment data to 1C for order id {$this->order->id}", $this->paymentOneCData);

        if (empty($this->paymentOneCData)) {
            Log::error("Payment data is empty");
            return;
        }

        if (!$this->canSendPaymentToOneC()) {
            Log::error("Can not send payment data to 1C");
            return;
        }

        $oneCOrder = app()->make(OneCOrder::class);
        $oneCResponse = $oneCOrder->sendPayment($this->paymentOneCData);

        Log::info("Response from 1C", ['response' => $oneCResponse]);

        if (!$oneCResponse ||
            !isset($oneCResponse['data']) ||
            !isset($oneCResponse['data']['Payment']) ||
            strtolower($oneCResponse['data']['Payment']) !== "ok"
        ) {
            Log::error("Error response from 1C");
            return;
        }

        $this->order->payment_synchronized_with_1c = true;
    }

    /**
     * Check if we can send the payment data to 1C
     *
     * @return bool
     */
    private function canSendPaymentToOneC(): bool
    {
        $requiredFields = $this->config['required_one_c_fields'] ?? [];
        $dependentFields = $this->config['dependent_one_c_fields'] ?? [];
        $missedRequiredFields = [];
        $missedDependentFields = [];
        $comment = $this->order->manager_comment;

        // check required fields
        if (!empty($requiredFields)) {
            $this->checkPaymentFields($requiredFields, $missedRequiredFields, $comment);
        }

        // check dependent fields
        if (!empty($dependentFields)) {
            $this->checkPaymentFields($dependentFields, $missedDependentFields, $comment, false);
        }

        $this->order->manager_comment = implode('', $missedRequiredFields) . implode('', $missedDependentFields) . $comment;

        if (!empty($missedRequiredFields) || !empty($missedDependentFields)) {
            Log::info('Missed fields', [
                'required' => $missedRequiredFields,
                'dependent' => $missedDependentFields
            ]);
        }

        return empty($missedRequiredFields) && count($missedDependentFields) !== count($dependentFields);
    }

    /**
     * Check payment fields if they are exists and not empty, generate text for each missed field
     *
     * @param $fields
     * @param $result
     * @param $comment
     * @param bool $isRequired
     */
    private function checkPaymentFields($fields, &$result, &$comment, bool $isRequired = true): void
    {
        $messages = [];

        foreach ($fields as $field) {
            if (!$message = $this->checkPaymentField($field, $comment)) {
                continue;
            }

            if (!is_bool($message)) {
                $messages[] = $message;
            }
        }

        if ($isRequired || count($fields) === count($messages)) {
            $result = $messages;
        }
    }

    /**
     * Check payment field, generate message text if field is empty or does not exist
     *
     * @param $field
     * @param $comment
     * @return bool|string
     */
    private function checkPaymentField($field, &$comment): bool|string
    {
        $paymentName = $this->config['name'] ?? '';
        $comment = str_replace(
            [
                trans('payment.missed_field', ['field' => $field, 'payment' => $paymentName]),
                trans('payment.missed_field', ['field' => $field, 'payment' => $paymentName]) . "\n"
            ],
            '',
            $comment
        );

        if (!empty($this->paymentInfo[$field])) {
            return true;
        }

        return trans('payment.missed_field', ['field' => $field, 'payment' => $paymentName]) . "\n";
    }

    /**
     * @param string $message
     * @param object|null $response
     * @return array
     */
    protected function generateErrorResponse(string $message, object $response = null): array
    {
        $output = [
            'success' => false,
            'message' => "Платежная система: {$this->paymentType->name_ru} \n"
        ];
        $output['message'] .= "ID заказа: {$this->order->id} \n";
        $output['message'] .= "ID оплаты: {$this->getPaymentOrderId()} \n";
        $output['message'] .= "Ошибка проведения операции: $message\n";

        if ($response && isset($response->err_code)) {
            $output['message'] .= "Код ошибки банка: {$response->err_code}.\n";
        }

        if ($response && isset($response->err_description)) {
            $output['message'] .= "Описание ошибки банка: {$response->err_description}.\n";
        }

        if ($response && isset($response->message)) {
            $output['message'] .= "Сообщение от банка: {$response->message}.\n";
        }

        Log::error($message, $output);

        return $output;
    }

    /**
     * @param array $data
     * @return array
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function setOrderHold(array $data = []): array
    {
        $this->order->payment->amount_holded = $data['amount'] ?? $this->order->total_price;

        if (!$this->order->payment->save()) {
            return $this->generateErrorResponse('Не удалось сохранить платеж');
        }

        $this->order->payment_status_id = PaymentStatus::getPaidStatusId();
        $this->order->who_updated = $this->getWhoUpdatedText();
        $this->order->payment_synchronized_with_1c = false;

        if (!$this->order->save()) {
            return $this->generateErrorResponse("Не удалось сохранить заказ после изменения статуса заказа");
        }

        return ['success' => true, 'message' => 'Операция успешна.'];
    }

    /**
     * @param int|null $sum
     *
     * @return array
     * @throws Exception
     */
    public function setOrderPayed(int $sum = null): array
    {
        $this->order->payment->hold_completion = true;
        $this->order->payment->amount_confirmed = $sum > 0 ? $sum : $this->order->payment->amount_holded;

        if (!$this->order->payment->save()) {
            return $this->generateErrorResponse('Не удалось сохранить платеж');
        }

        $this->order->payment_status_id = PaymentStatus::getPaidStatusId();
        $this->order->who_updated = $this->getWhoUpdatedText();

        if (!$this->order->save()) {
            return $this->generateErrorResponse('Не удалось сохранить заказ после изменения статуса заказа при подтверждении заказа');
        }

        return ['success' => true, 'message' => 'Операция успешна.'];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function setOrderNotPayed(): array
    {
        $this->order->payment_status_id = PaymentStatus::getWaitingForPaymentStatusId();
        $this->order->who_updated = $this->getWhoUpdatedText();
        $this->order->payment_synchronized_with_1c = false;

        if (!$this->order->save()) {
            return $this->generateErrorResponse("Не удалось сохранить заказ после изменения статуса заказа");
        }

        return ['success' => true, 'message' => 'Операция успешна.'];
    }
}

<?php

namespace App\Payment;

interface PaymentStrategy
{
    /**
     * Request bank to create payment for the order
     *
     * @param array $data
     * @return array
     */
    public function createPayment(array $data): array;

    /**
     * Get order payment status from bank
     *
     * @return array
     */
    public function getPaymentStatus(): array;

    /**
     * Process payment by request from the bank
     *
     * @param array $data
     * @return void
     */
    public function processPayment(array $data): void;

    /**
     * Request bank to hold the payment
     *
     * @return array
     */
    public  function holdCompletion(): array;

    /**
     * Request bank to refund the payment
     *
     * @return mixed
     */
    public function refundPayment();

    /**
     * Request bank to reject the payment
     *
     * @return mixed
     */
    public function rejectPayment();
}

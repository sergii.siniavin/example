<?php

namespace App\Payment;

use App\Models\OrderPayment;
use App\Models\PaymentStatus;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Log;
use LiqPay;

class LiqPayPayment extends BasePayment
{
    /**
     * @var LiqPay $liqPay
     */
    private LiqPay $liqPay;

    public function __construct()
    {
        parent::__construct();

        $this->config = config('payment.liqPay');
        $this->paymentReturnType = $this->config['payment_return_type'] ?: $this->paymentReturnType;
    }

    /**
     * @return array
     */
    public function getPaymentStatus(): array
    {
        if (!$paymentOrderId = $this->getPaymentOrderId()) {
            return $this->generateErrorResponse('Оплата не найдена в базе данных');
        }

        $this->apiInitialization();
        $data = [
            'action' => 'status',
            'order_id' => $paymentOrderId,
            'version' => '3'
        ];

        /**
         * @var object $response
         */
        $response = $this->liqPay->api('request', $data);

        $this->createLog([
            'description' => 'Отправили запрос на получение статуса',
            'request' => json_encode($data, JSON_UNESCAPED_UNICODE),
            'response' => is_string($response) ? $response : json_encode($response, JSON_UNESCAPED_UNICODE)
        ]);

        if (!$response || !is_object($response)) {
            return $this->generateErrorResponse('Не пришел ответ от банка.', $response);
        }

        if (!isset($response->result)) {
            return $this->generateErrorResponse('Пришел ответ от банка без поля result.', $response);
        }

        if ($response->result !== 'ok') {
            return $this->generateErrorResponse('Поле result не равно ok', $response);
        }

        return [
            'success' => true,
            'is_payed' => $response->status === 'success',
            'is_hold' => in_array($response->status, ['hold_wait', 'wait_accept']),
            'is_returned' => in_array($response->status, ['reversed', 'wait_reserve']),
            'data' => json_decode(json_encode($response, JSON_UNESCAPED_UNICODE), true)
        ];
    }

    /**
     * @param array $data
     * @return array{success:bool, type:string, data:string|array}
     */
    public function createPayment(array $data): array
    {
        $paymentData = [
            'action' => 'hold',
            'amount' => number_format($this->order->total_price, 2, '.', ''),
            'currency' => 'UAH',
            'description' => str_replace("[ORDER_NUMBER]", $this->order->order_id, $this->settings["description"] ?? ''),
            'order_id' => $this->getPaymentOrderId(),
            'version' => '3',
            'language' => $this->language === 'ua' ? 'uk' : $this->language,
            'server_url' => "$this->baseURL/payment/liqpay/process/",
            'result_url' => $data['redirect_url'],
        ];

        $this->apiInitialization();
        $html = $this->liqPay->cnb_form($paymentData);

        $this->createLog([
            'description' => 'Отправили запрос на получение формы оплаты',
            'request' => json_encode($paymentData, JSON_UNESCAPED_UNICODE),
            'response' => json_encode(['html' => $html], JSON_UNESCAPED_UNICODE)
        ]);

        return [
            'success' => strlen($html) > 0 && str_contains($html, '<form'),
            'type' => $this->paymentReturnType,
            'data' => $html
        ];
    }

    /**
     * @param array $data
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function processPayment(array $data): void
    {
        if (empty($data['signature'])) {
            Log::error("ProcessPayment: Запрос не содержит поле signature");
            return;
        }

        if (empty($data['data'])) {
            Log::error("ProcessPayment: Запрос не содержит поле data");
            return;
        }

        $paymentData = json_decode(base64_decode($data['data']), true);

        if (!$paymentData) {
            Log::error("ProcessPayment: Запрос не получается расшифровать");
            return;
        }

        if (!isset($paymentData['order_id'])) {
            Log::error("ProcessPayment: Запрос не содержит поле order_id");
            return;
        }

        $payment = $this->orderPaymentRepository->findByField('payment_order_id', $paymentData['order_id']);

        if (!$payment instanceof OrderPayment) {
            Log::error("ProcessPayment: Платеж {$paymentData['order_id']} не найден в базе данных");
            return;
        }

        $this->setOrder($payment->order);
        $this->createLog([
            'description' => 'Запрос из банка после оплаты клиента',
            'request' => json_encode($paymentData, JSON_UNESCAPED_UNICODE),
            'response' => null
        ]);
        $this->setPaymentSettings($payment->getPaymentSettings());

        $payment->data = $paymentData;
        if (!$payment->save()) {
            Log::error("ProcessPayment: Не удалось сохранить данные по платежу платеж");
            return;
        }

        $ourSignature = base64_encode(sha1("{$this->settings['private_key']}{$data['data']}{$this->settings['private_key']}", true));

        if ($data['signature'] !== $ourSignature) {
            Log::error("ProcessPayment: Запрос с неправильной подписью");
            return;
        }

        if (!isset($paymentData['status'])) {
            Log::error("ProcessPayment: Запрос не содержит поле status");
            return;
        }

        $successStatuses = ['success', 'hold_wait', 'wait_accept'];

        if (!in_array($paymentData['status'], $successStatuses)) {
            $expectedStatuses = json_encode($successStatuses);
            Log::error("ProcessPayment: Статус {$paymentData['status']} не подходит. Ожидаем {$expectedStatuses}");
            return;
        }

        if ($payment->id !== $this->order->payment->id) {
            Log::error("ProcessPayment: ID платежа {$payment->id} не равно ID текущего платежа заказа {$this->order->payment->id}. Данные платежа сохранены, статус заказа не обрабатывается.");
            return;
        }

        if ($paymentData['status'] === 'success') {
            $this->generatePaymentDataForOneC();
            $this->setOrderPayed($paymentData['amount'] ?? null);
        } else {
            $this->setOrderHold($paymentData);
        }
    }

    /**
     * @param int|null $sum
     * @return array
     * @throws Exception
     */
    public function holdCompletion(int $sum = null): array
    {
        if (!$paymentOrderId = $this->getPaymentOrderId()) {
            return $this->generateErrorResponse("Не удалось найти оплату по заказу {$this->order->id}");
        }

        $sum = $sum > 0 ? $sum : $this->order->total_price;
        $this->apiInitialization();

        $data = [
            'action' => 'hold_completion',
            'order_id' => $paymentOrderId,
            'amount' => number_format($sum, 2, '.', ''),
            'version' => '3'
        ];

        /**
         * @var object $response
         */
        $response = $this->liqPay->api('request', $data);

        $this->createLog([
            'description' => 'Отправили запрос на списание денег',
            'request' => json_encode($data, JSON_UNESCAPED_UNICODE),
            'response' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ]);

        if (!$response) {
            return $this->generateErrorResponse('Не пришел ответ от банка.', $response);
        }

        if (!isset($response->status)) {
            return $this->generateErrorResponse('Пришел ответ от банка без поля status.', $response);
        }

        if ($response->status !== 'success') {
            return $this->generateErrorResponse('Поле status не равно success', $response);
        }

        return $this->setOrderPayed($sum);
    }

    /**
     * @param int|null $sum
     * @return array
     * @throws Exception
     */
    public function rejectPayment(int $sum = null): array
    {
        if (!$this->payment) {
            return $this->generateErrorResponse("Переменная платежа не была инициализированна");
        }

        if (is_null($sum) && !is_null($this->payment->amount_holded)) {
            $sum = $this->payment->amount_holded;
        }

        if (is_null($sum)) {
            $sum = $this->order->total_price;
        }

        $this->apiInitialization();
        $data = [
            'action' => 'refund',
            'order_id' => $this->payment->payment_order_id,
            'amount' => number_format($sum, 2, '.', ''),
            'version' => '3'
        ];

        /**
         * @var object $response
         */
        $response = $this->liqPay->api('request', $data);

        $this->createLog([
            'description' => 'Отправили запрос на отмену платежа (reject)',
            'request' => json_encode($data, JSON_UNESCAPED_UNICODE),
            'response' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ]);

        $successStatuses = ['reversed', 'success'];

        if (!in_array($response->status, $successStatuses)) {
            return $this->generateErrorResponse('Банк вернул ошибку.', $response);
        }

        $this->payment->amount_returned = $sum;

        if (!$this->payment->save()) {
            return $this->generateErrorResponse('Не удалось сохранить платеж в базе данных после отмены платежа', $response);
        }

        if (!$paymentStatusId = PaymentStatus::getReturnedStatusId()) {
            return $this->generateErrorResponse('Не удалось найти статус оплаты с external_id 6', $response);
        }

        $this->order->payment_status_id = $paymentStatusId;
        $this->order->who_updated = $this->getWhoUpdatedText();

        if (!$this->order->save()) {
            return $this->generateErrorResponse('Не удалось сохранить заказ после отмены платежа', $response);
        }

        return [
            'success' => true,
            'message' => "Платеж {$this->payment->id} отменен."
        ];
    }

    /**
     * @param int|null $sum
     * @return array
     * @throws Exception
     */
    public function refundPayment(int $sum = null): array
    {
        if (!$this->payment) {
            return $this->generateErrorResponse("Переменная платежа не была инициализированна");
        }

        if (is_null($sum) && !is_null($this->payment->amount_confirmed)) {
            $sum = $this->payment->amount_confirmed;
        }

        if (is_null($sum)) {
            $sum = $this->order->total_price;
        }

        $this->apiInitialization();

        $data = [
            'action' => 'refund',
            'order_id' => $this->payment->payment_order_id,
            'amount' => number_format($sum, 2, '.', ''),
            'version' => '3'
        ];

        /**
         * @var object $response
         */
        $response = $this->liqPay->api('request', $data);

        $this->createLog([
            'description' => 'Отправили запрос на возврат денег (refund)',
            'request' => json_encode($data, JSON_UNESCAPED_UNICODE),
            'response' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ]);

        $successStatuses = ['reversed', 'success'];

        if (!in_array($response->status, $successStatuses)) {
            return $this->generateErrorResponse('Банк вернул ошибку.', $response);
        }

        $this->payment->amount_returned = $sum;

        if (!$this->payment->save()) {
            return $this->generateErrorResponse('Не удалось сохранить платеж в базе данных после возврата платежа', $response);
        }

        if (!$paymentStatusId = PaymentStatus::getReturnedStatusId()) {
            return $this->generateErrorResponse('Не удалось найти статус оплаты с external_id 6', $response);
        }

        $this->order->payment_status_id = $paymentStatusId;
        $this->order->who_updated = $this->getWhoUpdatedText();

        if (!$this->order->save()) {
            return $this->generateErrorResponse('Не удалось сохранить заказ после возврата денег', $response);
        }

        return [
            'success' => true,
            'message' => "Возврат платежа {$this->payment->id} произведен."
        ];
    }

    /**
     * @return string
     */
    public function getOrderPrefix(): string
    {

        if (empty($this->settings) || !isset($this->settings['order_prefix'])) {
            return '';
        }

        return $this->settings['order_prefix'] ?: '';
    }

    /**
     * @return bool
     */
    public function needsRedirectUrl(): bool
    {
        return true;
    }

    /**
     * @return void
     */
    private function apiInitialization(): void
    {
        $this->liqPay = new LiqPay($this->settings['public_key'], $this->settings['private_key']);
    }

    /**
     * Generate payment data body for request to 1C
     * @throws BindingResolutionException
     */
    public function generatePaymentDataForOneC()
    {
        Log::info("Preparing payment data for order {$this->order->id} to send to 1C");
        $this->order->payment_synchronized_with_1c = false;

        $this->paymentInfo = is_array($this->order->payment->data) ? $this->order->payment->data : [];
        $this->paymentOneCData = [
            "Payment" => [
                "NumberOrder" => isset($this->order->order_id) ? $this->order->order_id : "",
                "CardMask" => !empty($this->paymentInfo["sender_card_mask2"])
                    ? (string)$this->paymentInfo["sender_card_mask2"]
                    : "",
                "AuthCode" => !empty($this->paymentInfo["authcode_debit"])
                    ? (string)$this->paymentInfo["authcode_debit"]
                    : "",
                "PaymentSystem" => !empty($this->paymentInfo["sender_card_type"])
                    ? (string)$this->paymentInfo["sender_card_type"]
                    : "",
                "RRN" => !empty($this->paymentInfo["rrn_debit"])
                    ? (string)$this->paymentInfo["rrn_debit"]
                    : "",
                "Acquirer" => !empty($this->paymentInfo["acq_id"])
                    ? (string)$this->paymentInfo["acq_id"]
                    : "",
                "NameAcquirer" => !empty($this->paymentInfo["description"])
                    ? (string)$this->paymentInfo["description"]
                    : "",
                "PaymentIsSplit" => 0,
                "OperationType" => 1
            ]
        ];

        $this->sendPaymentToOneC();
    }
}

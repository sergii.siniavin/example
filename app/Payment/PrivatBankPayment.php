<?php

namespace App\Payment;

use App\Models\OrderPayment;
use App\Models\PaymentStatus;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Log;

class PrivatBankPayment extends BasePayment
{
    public function __construct()
    {

    }

    /**
     * @return array
     */
    public function getPaymentStatus(): array
    {

    }

    /**
     * @param array $data
     * @return array
     */
    public function createPayment(array $data): array
    {

    }

    /**
     * @param array $data
     * @return void
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function processPayment(array $data): void
    {

    }

    /**
     * @return array
     * @throws Exception
     */
    public function holdCompletion(): array
    {

    }

    /**
     * @return array
     * @throws Exception
     */
    public function rejectPayment(): array
    {

    }

    /**
     * @param int|null $sum
     * @return array
     * @throws Exception
     */
    public function refundPayment(int $sum = null): array
    {

    }

    /**
     * Generate payment data body for request to 1C
     * @throws BindingResolutionException
     */
    public function generatePaymentDataForOneC()
    {

    }

    /**
     * @return bool
     */
    public function needsRedirectUrl(): bool
    {

    }

    /**
     * @return array[]
     */
    private function generateProductsData(): array
    {

    }

    /**
     * @param string $url
     * @param array $data
     * @return mixed
     */
    private function request(string $url, array $data): mixed
    {

    }
}

<?php

namespace App\Services;

use App\Models\Order;
use App\Models\OrderPayment;
use App\Payment\BasePayment;
use App\Payment\InShopPayment;
use App\Payment\LiqPayPayment;
use App\Payment\MonoBankPayment;
use App\Payment\OnDeliveryPayment;
use App\Payment\PrivatBankPayment;
use App\Models\PaymentType;
use \Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Log;
use \InvalidArgumentException;

class PaymentService
{
    /**
     * @var ?Order $order
     */
    private ?Order $order = null;

    /**
     * @var BasePayment
     */
    private BasePayment $paymentModule;

    /**
     * Get order payment status from bank
     *
     * @return array
     * @throws Exception
     */
    public function getPaymentStatus(): array
    {
        if (is_null($this->order)) {
            throw new Exception('Order is not initialized.');
        }

        if (is_null($this->paymentModule->getPaymentOrderId())) {
            $this->setPaymentOrderId();
        }

        $result = $this->paymentModule->getPaymentStatus();

        if ($result['success'] === true) {
            $result['payment_name'] = $this->paymentModule->paymentType->name_ru;
            $result['order_id'] = $this->order->id;
            $result['order_payment_id'] = $this->paymentModule->getPaymentOrderId();
        }

        return $result;
    }

    /**
     * Request bank to create payment for the order
     *
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function createPayment(array $data): array
    {
        if (is_null($this->order)) {
            throw new Exception('Order is not initialized.');
        }

        $this->paymentModule->setPaymentOrderId($this->order->generatePaymentOrderId($this->paymentModule->getOrderPrefix()));

        $paymentData = [];
        if ($this->paymentModule->needsRedirectUrl() && isset($data['redirect_url'])) {
            $paymentData['redirect_url'] = $this->getRedirectURL($data['redirect_url']);
        }

        $response = $this->paymentModule->createPayment($paymentData);

        $this->order->addPayment($this->paymentModule->getPaymentOrderId());
        $this->order->linkPayment($this->paymentModule->getPaymentOrderId());

        return $response;
    }

    /**
     * Process payment by request from the bank
     *
     * @param array $data
     * @return void
     */
    public function processPayment(array $data): void
    {
        Log::info('Payment request from the bank', ['request' => $data]);
        $this->paymentModule->processPayment($data);
    }

    /**
     * Request bank to hold the payment
     *
     * @param int|null $sum
     * @return array
     * @throws Exception
     */
    public function holdCompletion(int $sum = null): array
    {
        if (is_null($this->order)) {
            throw new Exception('Order is not initialized.');
        }

        if (is_null($this->paymentModule->getPaymentOrderId())) {
            $this->setPaymentOrderId();
        }

        Log::info("Order {$this->order->id} hold completion running.");

        return $this->paymentModule->holdCompletion($sum);
    }

    /**
     * Request bank to reject the payment
     *
     * @param int|null $sum
     * @return array
     * @throws Exception
     */
    public function rejectPayment(int $sum = null): array
    {
        if (is_null($this->order)) {
            throw new Exception('Order is not initialized.');
        }

        if (!$this->paymentModule->getPayment()) {
            throw new Exception('Payment is not initialized.');
        }

        return $this->paymentModule->rejectPayment($sum);
    }

    /**
     * Request bank to refund the payment
     *
     * @param int|null $sum
     * @return array
     * @throws Exception
     */
    public function refundPayment(int $sum = null): array
    {
        if (is_null($this->order)) {
            throw new Exception('Order is not initialized.');
        }

        if (!$this->paymentModule->getPayment()) {
            throw new Exception('Payment is not initialized.');
        }

        return $this->paymentModule->refundPayment($sum);
    }


    /**
     * Initialize the order variable
     *
     * @param Order|int $order
     * @return void
     * @throws Exception
     */
    public function setOrder(Order|int $order): void
    {
        if ($order instanceof Order) {
            $this->order = $order;
            $this->setOrderPaymentModule();
            return;
        }

        $this->order = Order::find($order);

        if (!$this->order) {
            throw new Exception('Order not found');
        }

        $this->setOrderPaymentModule();
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * Initialize the payment module. Link order to payment module.
     *
     * @param int|null $paymentExternalID
     * @return void
     * @throws Exception
     */
    public function setOrderPaymentModule(int $paymentExternalID = null): void
    {
        if (is_null($this->order)) {
            throw new Exception('Order is not initialized.');
        }

        if ($paymentExternalID !== null) {
            $this->setPaymentModule($paymentExternalID);
        }

        if ($paymentExternalID === null) {
            $this->setPaymentModule($this->order->paymentType->external_id);
        }

        $this->paymentModule->setOrder($this->order);
    }

    /**
     * Determine the payment module
     *
     * @param int $paymentTypeExternalId
     * @return void
     * @throws Exception
     */
    public function setPaymentModule(int $paymentTypeExternalId): void
    {
        $this->paymentModule = match ($paymentTypeExternalId) {
            PaymentType::getLiqpayExternalID() => new LiqPayPayment(),
            PaymentType::getPrivatbankExternalID() => new PrivatBankPayment(),
            PaymentType::getMonoExternalID() => new MonoBankPayment(),
            PaymentType::getShopExternalID() => new InShopPayment(),
            PaymentType::getDepartmentExternalID() => new OnDeliveryPayment(),
            default => throw new InvalidArgumentException('Unknown payment type'),
        };
        $this->setPaymentModuleSettings($paymentTypeExternalId);
    }

    /**
     * Initialize payment module settings
     *
     * @param int $paymentTypeExternalId
     * @return void
     * @throws Exception
     */
    public function setPaymentModuleSettings(int $paymentTypeExternalId): void
    {
        $paymentType = PaymentType::getByField('external_id', $paymentTypeExternalId);
        $this->paymentModule->setPaymentType($paymentType);

        $settings = PaymentType::getSettings($paymentType);
        $this->paymentModule->setPaymentSettings($settings ?: []);
    }

    /**
     * @return BasePayment
     */
    public function getPaymentModule(): BasePayment
    {
        return $this->paymentModule;
    }

    /**
     * Generate redirect url. The user will be redirected to this URL when pay the order.
     *
     * @param string $page
     * @return string
     */
    private function getRedirectURL(string $page): string
    {
        $frontendUrl = config('app.frontend_url');
        $language = $this->paymentModule->getLanguage();

        return $language === 'ua'
            ? "{$frontendUrl}{$page}"
            : "{$frontendUrl}/{$language}{$page}";
    }

    /**
     * Initialize payment
     *
     * @param string|OrderPayment|null $paymentOrderId
     * @throws Exception
     */
    public function setPaymentOrderId(string|OrderPayment $paymentOrderId = null): void
    {
        if (is_null($paymentOrderId)) {
            $this->paymentModule->setPaymentOrderId($this->order->getPaymentOrderId($this->paymentModule->getOrderPrefix()));
            return;
        }

        if (is_string($paymentOrderId)) {
            $payment = $this->order->getPayment($paymentOrderId);
        }

        if ($paymentOrderId instanceof OrderPayment) {
            $payment = $paymentOrderId;
        }

        if (!isset($payment) || !$payment instanceof OrderPayment) {
            throw new Exception('Payment not found');
        }

        $this->setOrderPaymentModule($payment->paymentType->external_id);
        $this->paymentModule->setPayment($payment);
    }

    /**
     * Get payment data from bank, update payment status.
     *
     * @param bool $skipIfOrderPayed
     * @return Order
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function paymentStatusActualization(bool $skipIfOrderPayed = true): Order
    {
        if (is_null($this->order)) {
            throw new Exception('Order is not initialized');
        }

        if ($this->order->paymentStatus->isPayed() && $skipIfOrderPayed) {
            return $this->order;
        }

        $status = $this->getPaymentStatus();

        if (isset($status['is_payed']) && $status['is_payed']) {
            $this->paymentModule->setOrderPayed();
            return $this->order;
        }

        if (isset($status['is_hold']) && $status['is_hold']) {
            $this->paymentModule->setOrderHold($status['data']);
            return $this->order;
        }

        $this->paymentModule->setOrderNotPayed();
        return $this->order;
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Payment
{
    /**
     * Handle an incoming payment request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        Log::info('Payment request', [
            'request'  => $request->all(),
            'headers'  => $request->headers
        ]);

		return $next($request);
    }
}

<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class BaseApiController extends Controller
{
    /**
     * Generate body and return success response
     *
     * @param JsonResource|array $result
     * @param string $message
     * @return JsonResponse|JsonResource
     */
    public function sendResponse(JsonResource|array $result, string $message = ''): JsonResponse|JsonResource
    {
        $response = $this->makeResponse($result, $message);
        return is_array($response) ? Response::json($response) : $response;
    }

    /**
     * Collect success response body
     *
     * @param string $message
     * @param JsonResource|array $data
     * @return JsonResource|array
     */
    public function makeResponse(JsonResource|array $data, string $message): array|JsonResource
    {
        $additionalData = [
            'success' => true,
            'message' => $message,
        ];

        if ($data instanceof JsonResource) {
            $data->with = array_merge($additionalData, $data->with);
        }

        if (is_array($data)) {
            $data = array_merge($additionalData, $data);
        }

        return $data;
    }

    /**
     * Generate body and return error response
     *
     * @param string $error
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    public function sendError(string $error, array $data = [], int $code = HttpFoundationResponse::HTTP_NOT_FOUND): JsonResponse
    {
        return Response::json($this->makeError($error, $data), $code);
    }

    /**
     * Collect error response body
     *
     * @param string $message
     * @param array  $data
     *
     * @return array
     */
    public function makeError(string $message, array $data = []): array
    {
        $body = [
            'success' => false,
            'message' => $message,
        ];

        if (!empty($data)) {
            $body = array_merge($body, $data);
        }

        return $body;
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePaymentRequest;
use App\Services\PaymentService;
use Illuminate\Http\JsonResponse;
use \Exception;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class PaymentController extends BaseApiController
{
    /**
     * Create payment request processing
     *
     * @param CreatePaymentRequest $request
     * @return JsonResponse|JsonResource
     */
    public function create(CreatePaymentRequest $request): JsonResponse|JsonResource
    {
        try {
            $data = [
                'redirect_url' => config('app.frontend_order_success_page', '/order-success')
            ];

            $paymentService = new PaymentService();
            $paymentService->setOrder($request->order_id);
            $result = $paymentService->createPayment($data);

            return $this->sendResponse($result);
        }
        catch (Exception $exception) {
            Log::error($exception->getMessage(), $exception->getTrace());

            return $this->sendError('Something went wrong');
        }
    }
}

<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Services\PaymentService;
use Exception;

class BaseController extends Controller
{
    /**
     * Payment type ID for payment module initialization
     *
     * @var int $paymentTypeId
     */
    protected int $paymentTypeId = 0;

    /**
     * Payment request processing
     *
     * @param array $data
     * @return void
     * @throws Exception
     */
    protected function process(array $data): void
    {
        if($this->paymentTypeId === 0) {
            throw new Exception('Payment type ID is not initialized');
        }

        $paymentService = new PaymentService();
        $paymentService->setPaymentModule($this->paymentTypeId);
        $paymentService->processPayment($data);
    }
}

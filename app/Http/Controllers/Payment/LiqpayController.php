<?php

namespace App\Http\Controllers\Payment;

use App\Http\Requests\Payment\Liqpay;
use App\Models\PaymentType;
use Exception;
use Illuminate\Support\Facades\Log;

class LiqpayController extends BaseController
{
    /**
     * Process Liqpay request from the bank
     *
     * @param Liqpay $request
     * @return void
     */
    public function processPayment(Liqpay $request): void
	{
        Log::info("Liqpay request processing");

        try {
            $data = $request->all();
            $this->paymentTypeId = PaymentType::getLiqpayExternalID();
            $this->process($data);
        }
        catch (Exception $exception) {
            Log::error($exception->getMessage(), $exception->getTrace());
        }
	}
}

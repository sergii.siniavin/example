<?php

namespace App\Http\Controllers\Payment;

use App\Http\Requests\Payment\Mono;
use App\Models\PaymentType;
use Exception;
use Illuminate\Support\Facades\Log;

class MonoController extends BaseController
{
    /**
     * Process Mono request from the bank
     *
     * @param Mono $request
     * @return void
     */
    public function processPayment(Mono $request): void
    {
        Log::info("Mono request processing");

        try {
            $data = $request->all();
            $this->paymentTypeId = PaymentType::getMonoExternalID();
            $this->process($data);
        }
        catch (Exception $exception) {
            Log::error($exception->getMessage(), $exception->getTrace());
        }
    }
}

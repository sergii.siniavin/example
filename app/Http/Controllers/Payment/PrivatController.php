<?php

namespace App\Http\Controllers\Payment;

use App\Http\Requests\Payment\Privat;
use App\Models\PaymentType;
use Exception;
use Illuminate\Support\Facades\Log;

class PrivatController extends BaseController
{
    /**
     * Process Privat request from the bank
     *
     * @param Privat $request
     * @return void
     */
    public function processPayment(Privat $request): void
    {
        Log::info("Privatbank request processing");

        try {
            $data = $request->all();
            $this->paymentTypeId = PaymentType::getPrivatbankExternalID();
            $this->process($data);
        }
        catch (Exception $exception) {
            Log::error($exception->getMessage(), $exception->getTrace());
        }
	}
}
